ARPOADOR is a simple tool for capturing and analyzing Broadcast packets,
especially ARP on IPv4 ATM Vlan and Neighbor Discovery on IPv6 ATM Vlan,
on Internet exchange points.

The concept of ARPOADOR it to be divided in 3 phases:
 1st -> Receive the broadcasts
 2nd -> Process the broadcasts e create statistiscs files(csv).
 3rd -> Delivery data, create informations.

Our intention is to make the requirements as basic as possible.
For now the requirements are only:
- Linux (or unix like) with basic pipeling editing tools
  (awk, sed, top, sort, unique, etc ...)
- "tshark" package installed, with "libpcap" library,
  and permission for the user to make non-elevation catches
   - Usually "#sudo gpasswd -a $ USER wireshark" solves it.

"arpoador.sh"
Is the base shell scrpit that will do the capture and call the
secondary scrpits that will process the statistics and publish the
informations.

"arpoador.conf"
is the configuration file where informations used to do captures and create
the statistics are inputed.
The variables ont it are very self explained. Improve the comments to make it
more no-brainer for the users is a work in progress.

Describing the pahses os ARPOADOR
1st -> Receive the broadcasts
 	By now we validated only methods that are:
		a) Directly sniffing the vlans, where vlans must be bridged to a
		interface of the host running ARPOADOR
		b) Stream with TZSP (Mikrotik and other vendors), where it encapsulates
		the packets specified by some filter and stream to a specified ip
		address.
	We are working other two methods:
		c) Pseudowire (Cisco, Juniper, etc) directly stablished with linux host
		running ARPOADOR.
		d) EoIP (Mikrotik) directly stablished with linux host running
		ARPOADOR.

2nd -> Process the broadcasts e create statistiscs files(csv).
		arpoador.sh -> central scrpit thar will call the other scripts.

3rd -> Delivery data, create informations.
		Based on csv files generated on 2nd phase we will provide those
		informations in two formats:
		- SNMP
		the created data will be reachable for any SNMP compatible tool on the
		market(Nagios, Zabbix, Observium, etc...) and with taht creanting any
		kind of triggers.
		- HTML
		A very simple way to present the last 60 collects on HTML frames and
		sheets, highlighting the over limit packet emiters.
		