#!/bin/bash
#Arpoador Script - Writen By fischerdouglas@gmail.com v0.6 2018-05-14

#To-Do
#Test If User variables are valid.
#Test If needed Apps and Packages are installed and avaliable
#Test If Tshark Can Capture (Defined in installation - and User Group)
#Create Sum of packets
#  awk -F"," '{x+=$3}END{print "Total " x}' $StatistcsFileName-v4-NormalArpBySource.csv
#Prepare ASN and ASN_Name in Statistiscs File


#Receive configfile from CLI call, or define default configfile.
if [ -f "$1" ]; then configfile=$1; else   configfile='arpoador.conf'; fi

#Remove basic malicious entry on configfile
configfile_secured='/tmp/arpoador.conf.secure'
egrep '^#|^[^ ]*=[^;&]*'  "$configfile" > "$configfile_secured"

#Get User defined variables from configuration file
source $configfile_secured

#Script Defined Variables
TimeStamp=`date +%Y%m%d%H%M`
BaseCaptureFile="${CaptureFilesDirectory}capture-${IXName}-${TimeStamp}"
StatistcsFile="${StatisticsFilesDirectory}statistics-${IXName}-${TimeStamp}"

#Test existency of Working Directories
if [ ! -d "${CaptureFilesDirectory}" ]; then mkdir -p ${CaptureFilesDirectory}; fi
if [ ! -d "${StatisticsFilesDirectory}" ]; then mkdir -p ${StatisticsFilesDirectory}; fi

#Define if capture come from Stream(TZSP) or Directly
if [ ${ReceiveFromStream} = true  ]
    then
        StreamFilterString="host ${StreamSenderIPAddress} and udp port ${StreamDestinationPort}"
    else
        StreamFilterString=""
fi



#Does the Capture on the Interface where comes the bridged traffic from IX ATM Vlans
tshark -q  -a duration:$CaptureDuration -i $InterfaceOfCapture \
       -w $BaseCaptureFile.pcap -F pcap $StreamFilterString



# Deal with IPv4 Requests - Expected packets
if [ ${ProcessIPv4ARPStatistics} = true ]; then

    #Split Cature File
    tshark -r $BaseCaptureFile.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID \
                and arp.opcode == 1 and not arp.isgratuitous \
                and arp.src.proto_ipv4 == $IXATMIPv4_Net \
                and arp.dst.proto_ipv4 == $IXATMIPv4_Net""" \
        -w $BaseCaptureFile-IPv4.pcap

    ## Creating Statistcs Files
    #List Requested
    tshark -r $BaseCaptureFile-IPv4.pcap \
        -o 'gui.column.format:"""IPv4-Requested""","""%Cus:arp.dst.proto_ipv4"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1}'\
        | sort -t, -k2 -n -r > $StatistcsFile-IPv4-NormalByArpDestination.csv
    sed -i '1s/^/IPv4-Requested,Quantity\n/' $StatistcsFile-IPv4-NormalByArpDestination.csv

    #List Requeters
    tshark -r $BaseCaptureFile-IPv4.pcap \
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""IPv4-Source""","""%Cus:arp.src.proto_ipv4""","""MacEUI-Source""","""%rhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""","""$3""","""$4}'\
        | sort -t, -k2 -n -r > $StatistcsFile-IPv4-NormalByArpSource.csv
    ##
    ##Put headers on CSV
    sed -i '1s/^/Mac-Source,Quantity,IPv4-Source,MacEUI-Source\n/' $StatistcsFile-IPv4-NormalByArpSource.csv
	
	##List Destination with Shapping
	##
	## tshark -r ./data/IX-BR-SP/captures/capture-IX-BR-SP-201806011808-IPv4.pcap -o 'gui.column.format:"""Timestamp""","""%t""","""Mac-Source""","""%uhs""","""IPv4-Reqested""","""%Cus:arp.dst.proto_ipv4"""' | sort -t" " -k2 -k3 -k1 -V


	# RemoveCaptureFiles
	if [ ${RemoveCaptureFiles} = true ]; then
		rm $BaseCaptureFile-IPv4.pcap
	fi

fi

# Deal with IPv6 Requests - Expected packets
if [ ${ProcessIPv6NDStatistics} = true ]; then

    #Split Cature File
    tshark -r $BaseCaptureFile.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID \
                and icmpv6.nd.ns.target_address == $IXATMIPv6_Net""" \
        -w $BaseCaptureFile-IPv6.pcap

    ## Creating Statistcs Files
    # List Requested
    tshark -r $BaseCaptureFile-IPv6.pcap \
        -o 'gui.column.format:"""IPv6-Requested""","""%Cus:icmpv6.nd.ns.target_address"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1}'\
        | sort -t, -k2 -n -r > $StatistcsFile-IPv6-NormalByNDDestination.csv
    sed -i '1s/^/IPv6-Requested,Quantity\n/' $StatistcsFile-IPv6-NormalByNDDestination.csv

    # List Requeters
    tshark -r $BaseCaptureFile-IPv6.pcap \
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""IPv6-Source""","""%Cus:ipv6.src""","""MacEUI-Source""","""%rhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""","""$3""","""$4}'\
        | sort -t, -k2 -n -r > $StatistcsFile-IPv6-NormalByNDSource.csv
    ##
    ##Put headers on CSV
    sed -i '1s/^/Mac-Source,Quantity,IPv6-Source,MacEUI-Source\n/' $StatistcsFile-IPv6-NormalByNDSource.csv

	# RemoveCaptureFiles
	if [ ${RemoveCaptureFiles} = true ]; then
		rm $BaseCaptureFile-IPv6.pcap
	fi

fi

# Deal with Non Conform Packets
if [ ${ProcessNonConformitiesStatistics} = true ]; then

    #Split Cature File
    tshark -r $BaseCaptureFile.pcap \
        -Y """((vlan.id == $IXATMIPv4_VlanID \
                and ! (arp.opcode == 1 and not arp.isgratuitous \
                        and arp.src.proto_ipv4 == $IXATMIPv4_Net \
                        and arp.dst.proto_ipv4 == $IXATMIPv4_Net)) \
            or (vlan.id == $IXATMIPv6_VlanID\
                and not icmpv6.nd.ns.target_address == $IXATMIPv6_Net))""" \
        -w $BaseCaptureFile-NonConform.pcap

    ## Creating Statistcs File
    # ATMv4 - Emiting QinQ
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID and vlan.etype == 0x8100""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv4,QinQ,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv6 - Emiting QinQ
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID and vlan.etype == 0x8100""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,QinQ,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    #
    # ATMv4 - Emiting Bootp
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID and bootp""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""Protocol""","""%p"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv4,Bootp,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv4 - Emiting Bootp
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID and bootp""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""Protocol""","""%p"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,Bootp,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    #
    # ATMv4 - Emiting Vendor Dicovery Protocol
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID and (mndp or cdp or lldp)""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""Protocol""","""%p"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv4,VendorDiscovery,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv6 - Emiting Vendor Dicovery Protocol
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID and (mndp or cdp or lldp)""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""Protocol""","""%p"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,VendorDiscovery,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    #
    # ATMv4 - Emiting Other then BGP Routing Protocols
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID and (rip or ripng or ospf or isis or eigrp or igrp)""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""Protocol""","""%p"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv4,RoutingProtocol,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv6 - Emiting Other then BGP Routing Protocols
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID and (rip or ripng or ospf or isis or eigrp or igrp)""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""Protocol""","""%p"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,RoutingProtocol,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    #
    # ATMv4 - Emiting IPv6
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID and ipv6 and not stp""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv4,IPv6onATMv4,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv6 - Emiting IPv4
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID and not ipv6 and not stp""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,IPv4onATMv6,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    #
    # ATMv4 - Emitting Gratuitous ARP
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID and arp.isgratuitous and arp.src.proto_ipv4 == $IXATMIPv4_Net""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""IP-Source""","""%Cus:arp.src.proto_ipv4"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv4,GratuitousArp,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv6 - Emitting Router Advertisement Packets
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID and icmpv6.type == 134""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,RouterAdvertisement,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    #
    # ATMv4 - Emitting packets with source out of the network range onf the ATM
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID and arp.opcode == 1 and not arp.src.proto_ipv4 == $IXATMIPv4_Net""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""IP-Source""","""%Cus:arp.src.proto_ipv4"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv4,ARPSourceOutOfRange,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv6 - Emitting packets with source out of the network range onf the ATM
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID and icmpv6.type == 135\
                and not ipv6.src == fe80::/64\
                and not ipv6.src == $IXATMIPv6_Net"""\
        -o 'gui.column.format:"""Mac-Source""","""%uhs""","""IPv6-Source""","""%Cus:ipv6.src"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,NDSourceOutOfRange,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    #
    # ATMv4 - List Arp Requesters those are asking by destination that are out of the network range of ATM
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID and arp.opcode == 1 and not arp.isgratuitous\
                and arp.src.proto_ipv4 == $IXATMIPv4_Net\
                and not arp.dst.proto_ipv4 == $IXATMIPv4_Net""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv4,ARPDstOutOfRange,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv6 - Neighbor Discovery asking by destination that are out of the network range of ATM
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID and icmpv6.type == 135\
                and not icmpv6.nd.ns.target_address == $IXATMIPv6_Net\
                and not icmpv6.nd.ns.target_address == fe80::/64""" \
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,NDTargetOutOfRange,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv6 - Neighbor Discovery asking by destination FE80
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID and icmpv6.type == 135\
                and icmpv6.nd.ns.target_address == fe80::/64"""\
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,NDTargetFE80,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv4 - NTP Multicat
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID and ntp and eth.ig == 1"""\
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv4,NTPMulticast,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    # ATMv6 - NTP Multicat
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID and ntp and eth.ig == 1"""\
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,NTPMulticast,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    #
    # ATMv4 - ICMP Broadcast
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv4_VlanID and icmp and eth.ig == 1"""\
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv4,ICMPBroadcast,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    #
    # ATMv6 - Emitting Packets multicast Related
    tshark -r $BaseCaptureFile-NonConform.pcap \
        -Y """vlan.id == $IXATMIPv6_VlanID\
                and (pim or icmpv6.type == 130 or icmpv6.type == 131 or icmpv6.type == 143)"""\
        -o 'gui.column.format:"""Mac-Source""","""%uhs"""'\
        | sort | uniq -c | awk -F ' ' '{print $2""","""$1""",ATMv6,MulticastRelated,"""$3}'\
        >> $StatistcsFile-NonConform.csv
    ##
    ##Put headers on CSV
    sed -i '1s/^/Mac-Source,Quantity,Vlan,TypeOfNonformity,Complement\n/' $StatistcsFile-NonConform.csv

	# RemoveCaptureFiles
	if [ ${RemoveCaptureFiles} = true ]; then
		rm $BaseCaptureFile-NonConform.pcap
	fi

fi


# RemoveCaptureFiles
if [ ${RemoveCaptureFiles} = true ]; then
	rm $BaseCaptureFile.pcap
fi
